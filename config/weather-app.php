<?php
/*
 * Weather App config values
 *
 */
return[
    /*
     * Locations for data retrieval and retrieval intervals
     * Data retrieval interval specified in cron format (i.e. run 15 minutes after the hour - '15 * * * *')
     * NULL value for interval reverts to the default interval
     *
     */
    'locations' => [

        'New York' => [
            'zip' => '10001',
            'interval' => '*/1 * * * *' // run every minute
        ],
        'Atlanta' => [
            'zip' => '30301',
            'interval' => '*/2 * * * *' // run every 2 minutes
        ],
        'Denver' => [
            'zip' => '80110',
            'interval' => '*/2 * * * *' // run every 2 minutes
        ],
        'Phoenix' => [
            'zip' => '85001',
            'interval' => NULL,         // use default interval
        ],
        'Tryon' => [
            'zip' => '28782',
            'interval' => NULL          // use default interval
        ],
        'San Francisco' => [
            'zip' => '94110',
            'interval' => '*/4 * * * *' // run every 4 minutes
        ]
    ],

    /*
     * Default data retrieval interval
     *
     */
    'default_interval' => '*/3 * * * *',    // run every 3 minutes

    /*
     * Remote Api settings
     *
     */
    'weatherApi' => [
        'endpoint' => 'http://api.openweathermap.org/data/2.5/weather',
        'keyParam' => 'APPID',
        'key' => 'c539d24a9513b375810d702eee61cfff',
        'lookupParam' => 'zip',
        'units' => 'imperial',

        /*
         * Max number of times to retry failed Api calls
         *
         */
        'maxTries' => 3,

        /*
         * Max seconds to wait for an Api response
         *
         */
        'timeOut' => 120,

        /*
         * Seconds to delay between failed Api calls
         *
         */
        'retryAfter' => 3
    ]
];
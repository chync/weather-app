# Laravel Weather App README #

This app was built in Laravel 5.4

### Requirements ###

Create an application that will track current weather measurements for a given set of zip codes

Store the following data at a minimum:

1. Zip code,
2. General weather conditions (e.g. sunny, rainy, etc),
3. Atmospheric pressure,
4. Temperature (in Fahrenheit),
5. Winds (direction and speed),
6. Humidity,
7. Timestamp (in UTC)

There is no output requirement for this application, it is data retrieval and storage only
The application should be able to recover from any errors encountered
The application should be developed using a TDD approach. 100% code coverage is not required
The set of zip codes and their respective retrieval frequency should be contained in configuration file
Use the OpenWeatherMap API for data retrieval (https://openweathermap.org)

### Design Overview ###

I chose the Laravel PHP framework (https://laravel.com/docs/5.4/installation) to build the app in order to take advantage of Laravel's scheduler and job queue which allow the app to run as a background process. These two functions provide the basis for being able to easily schedule recurring jobs/tasks. The Laravel queue makes it easy to dispatch jobs that can run in the background. If a job fails to run it can be automatically re-queued to run after a specified time period and failed jobs can be recorded and re-run at a later date. There are no routes or controllers for the app, everything runs as a background process.

The app is started by running the scheduler from the server command line.

The scheduler creates a unique job to fetch weather data for each location specified in the config/weather-app.php config file.

The config file includes an array of locations and data collection intervals in a cron format. The config file also holds the connection and error handling settings for the remote Api where the data is fetched.

A seperate Laravel process controls the job queue and it is also started from the server command line.

The jobs make Api calls to the weather site using the dependency GuzzleHTTP (http://docs.guzzlephp.org/en/stable/) as the HTTP client.

If a dispatched job fails the reason is logged in the storage/logs/laravel.log file. Failed jobs will be retried after the delay specified in the config file and will be retried up to the config setting maxTries. A timeout value can also be specified to halt the job if there is a delayed response from the remote Api.

The data retrieved is stored in a database.

The app was developed and tested on PHP 7.


### Install and Setup ###

Pull the app to an a web server. The Apache document root should point to the app folder "public".

Laravel uses Composer (https://getcomposer.org/) to manage dependencies. It must be installed to update the dependency packages.

From the App root run the following command to download the dependencies:

`composer update`

#### Environment and Database Settings ####

Copy the environment config file ".env.example" to ".env" in the app root.

Create a database schema and user and then update the database settings in the environment file accordingly.

Update the APP_URL parameter in the ".env" file.

#### Database Migration ####

From the app root command line setup the database tables by running:
 
 `php artisan migrate`
  
 (assumes php is in your system path).

#### App Config Settings ###

Edit the file config/weather-app.php to set the locations to collect data from and the Api settings.

The data collection intervals must be a valid cron format (5 column style). There is a default interval setting that is used if a location specific setting is set to Null.

The default interval is every 3 minutes: `*/3 * * * *`.

#### Starting the Scheduler ####

Add the following to the web server crontab: 

`* * * * * /<path to PHP>/php /<path to App root>/artisan schedule:run >> /dev/null 2>&1`

#### Starting the Queue Worker ####

Run the following command from the App root:

`<path to PHP>/php artisan queue:work`

### App Functionality Details ###

| Item  | Location |
| ------------- | ------------- |
| Environment Settings:| .env (copied from .env.example) |
| App Settings: | config/weather-app.php |
| Data Collection and Storage Job Class: | app/Jobs/GetWeatherDataByZip.php |
| Scheduler Class: | app/Console/Kernel.php |
| Artisan Command Class for Starting the Job Class: | app/Console/Commands/GetWeatherData.php |
| Log File: | storage/logs/laravel.log |
| Feature and Unit Tests: | tests/ |

### Testing ###

There isn't a great way to unit test the scheduler.

I started with a Feature test at the command level (the custom Artisan command that triggers the jobs).

You can run the phpunit test at:

`vendor/bin/phpunit`

This will create a job for the zip code in the test. The test will return true if the job was added to the job queue table "jobs".
 
If the job queue is not running the job will not be dispatched and will sit in the table until it is run using:

`php artisan queue:work`

You can also run the job command directly from the command line with a zip code as the parameter:

`php artisan weather:get <zip code>`
<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Validation\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;


class Kernel extends ConsoleKernel
{

    private $locations;
    private $location;
    private $location_name;
    private $job_interval;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        /**
         * Command to create and dispatch data collection job
         */
        Commands\GetWeatherData::class

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        /**
         * Create schedule of data collection jobs from config locations array
         *
         */
        $this->locations = config('weather-app.locations');

        foreach($this->locations as $this->location_name => $this->location){

            /**
             * Set job schedule interval
             */
            $this->job_interval = (is_null($this->location['interval']) ? config('weather-app.default_interval') : $this->location['interval']);

            /**
             * Validate zip code format
             */
            if(!$this->validateZip($this->location['zip'])) {

                /**
                 * Skip job - invalid zip format
                 */
                $log_msg = 'Invalid zip for location ' . $this->location_name . ':' . $this->location['zip'] . ' - Can\'t schedule job.';
                Log::error($log_msg);
                continue;

            }

            /**
             * Validate schedule interval format
             */
            if(!$this->validateInterval($this->job_interval)) {

                /**
                 * Skip job - invalid schedule format
                 */
                $log_msg = 'Invalid job schedule interval for location ' . $this->location_name . ':' . $this->location['zip'] . ' - Can\'t schedule job.';
                Log::error($log_msg);
                continue;

            }

            /**
             * Create job schedule
             */
            $schedule->command('weather:get ' . $this->location['zip'])
                ->cron($this->job_interval)
                ->name($this->location_name . ':' . $this->location['zip'])
                ->withoutOverlapping();

            Log::debug('Scheduling job for location: ' . $this->location_name . ':' . $this->location['zip']);

        }

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }

    /**
     * Validate job schedule interval
     * Requires a valid cron format
     *
     * @param $cron_param
     * @return mixed
     */
    private function validateInterval($cron_param){

        $cron_regex = "/^(((([\*]{1}){1})|((\*\/){0,1}(([0-9]{1}){1}|(([1-5]{1}){1}([0-9]{1}){1}){1}))) ((([\*]{1}){1})|((\*\/){0,1}(([0-9]{1}){1}|(([1]{1}){1}([0-9]{1}){1}){1}|([2]{1}){1}([0-3]{1}){1}))) ((([\*]{1}){1})|((\*\/){0,1}(([1-9]{1}){1}|(([1-2]{1}){1}([0-9]{1}){1}){1}|([3]{1}){1}([0-1]{1}){1}))) ((([\*]{1}){1})|((\*\/){0,1}(([1-9]{1}){1}|(([1-2]{1}){1}([0-9]{1}){1}){1}|([3]{1}){1}([0-1]{1}){1}))|(jan|feb|mar|apr|may|jun|jul|aug|sep|okt|nov|dec)) ((([\*]{1}){1})|((\*\/){0,1}(([0-7]{1}){1}))|(sun|mon|tue|wed|thu|fri|sat)))$/";

        return preg_match($cron_regex,$cron_param);

    }
    /**
     * Validate zip code
     *
     * @param $cron_param
     * @return mixed
     */
    protected function validateZip($zip){

        $zip_regex = "/^\d{5}(?:[-\s]\d{4})?$/";

        return preg_match($zip_regex,$zip);

    }
}

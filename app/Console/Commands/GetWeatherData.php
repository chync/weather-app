<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;
use App\Jobs\GetWeatherDataByZip;
use App\Exceptions\Handler;


class GetWeatherData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:get {zip : A valid Zip Code}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve weather data by zip code.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /**
         * Add a new job to the queue and dispatch
         */
        $this->job = new GetWeatherDataByZip($this->argument('zip'));

        dispatch($this->job);

        Log::debug('Dispatching job for location: ' . $this->argument('zip'));

    }

}

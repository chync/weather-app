<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs\GetWeatherData;
use Carbon\Carbon;

class weatherDataController extends Controller
{
    public function __invoke($zip=NULL)
    {
        if(is_null($zip)) {

            throw new Exception('Invalid zip code value.');

        }

        //$weather_data = new WeatherDataModel;

        $job = (new GetWeatherData($zip));
        $schedule->call(function() {
            //dispatch(new GetWeatherDataByZip($this->location['zip']));
            dispatch($job);
        })->everyMinute()->name($this->name . ':' . $this->location['zip'])->withoutOverlapping(); //cron($this->job_interval);*/
    }
}

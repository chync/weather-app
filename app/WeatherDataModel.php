<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeatherDataModel extends Model
{

    protected $table = 'weather_data';
    protected $connection = 'mysql';

}

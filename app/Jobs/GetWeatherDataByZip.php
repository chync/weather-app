<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Config;
use Mockery\Exception;
use App\WeatherDataModel;


class GetWeatherDataByZip implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $zip;
    public $tries;
    public $timeout;
    public $retry_after;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($zip)
    {

        $this->zip = $zip;

        /**
         * The number of times the job may be attempted.
         *
         * @var int
         */
        $this->tries = config('weather-app.weatherApi.maxTries');;

        /**
         * The number of seconds the job can run before timing out.
         *
         * @var int
         */
        $this->timeout = config('weather-app.weatherApi.timeOut');

        /**
         * The number of seconds between failed job attempts.
         *
         * @var int
         */
        $this->retry_after = config('weather-app.weatherApi.retryAfter');

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if(is_null($this->zip)){

            throw new Exception('Missing required input parameter: zip.');

        }

        Log::debug('Executing new data collection job for location ' . $this->zip);

        /**
         * Create new Guzzle HTTP client
         */
        $http_client = new Client();

        /**
         * Set Api endpoint
         */
        $endpoint = config('weather-app.weatherApi.endpoint')
            . '?units=' . config('weather-app.weatherApi.units')
            . '&' . config('weather-app.weatherApi.lookupParam') . '=' . $this->zip;

        try {

            /**
             * Fetch data from Api
             */
            $response = $http_client->request('GET', $endpoint
                . '&' . config('weather-app.weatherApi.keyParam') . '=' . config('weather-app.weatherApi.key')
                , ['allow_redirects' => false
                , 'http_errors' => true] );

            $response_body = json_decode($response->getBody());

            $response_body->zip = $this->zip;

            /**
             * Save to database
             */
            $this->save($response_body);

        } catch (ClientException $error) {

            /**
             * Handle Api errors
             */
            $response = $error->getResponse();

            $response_code = $response->getStatusCode();

            $response_reason = $response->getReasonPhrase();

            switch(substr($response_code,0,1)){

                case '3':
                    /**
                     * Do something unique with Redirect Errors
                     */
                    $error_type = 'REDIRECT';
                    break;

                case '4':
                    /**
                     * Do something unique with Client Errors
                     */
                    $error_type = 'CLIENT';
                    break;

                case '5':
                    /**
                     * Do something unique with Server Errors
                     */
                    $error_type = 'SERVER';
                    break;

            }

            $log_msg = $error_type . ' ERROR: ' . $response_code . ' - ' . $response_reason . ' for request ' . $endpoint . ' - Attempt ' . $this->attempts() . ' of ' . $this->tries . ' - Retry in ' . $this->retry_after . ' seconds.';
            Log::error($log_msg);

            /**
             * Retry the failed job after retry delay
             */
            if( $this->attempts() < $this->tries ) {

                $this->release($this->retry_after);

            } else {

                /**
                 * Abort failed job
                 */
                //$this->delete();

                $log_msg = 'ABORTING FAILED JOB for request ' . $endpoint;

                Log::error($log_msg);

                abort($response_code, $log_msg);

            }

        }

    }

    /**
     * Save the data
     *
     * @return void
     */
    private function save($data) {

        $weather_data = new WeatherDataModel;

        $weather_data->name = $data->name;
        $weather_data->zip = $data->zip;
        $weather_data->conditions = $data->weather[0]->description;
        $weather_data->conditions_short = $data->weather[0]->main;
        $weather_data->pressure = $data->main->pressure;
        $weather_data->temperature = $data->main->temp;
        $weather_data->wind_direction = $data->wind->deg;
        $weather_data->wind_speed = $data->wind->speed;
        $weather_data->humidity = $data->main->humidity;

        $weather_data->save();

    }

}

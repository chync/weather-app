<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Console\Command;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GetWeatherCommandTest extends TestCase
{
    /**
     * Test the artisan command weather:get {zip} that launches the jobs to fetch remote data
     *
     * @return void
     */
    public function testArtisanCommandWeather()
    {
        $this->artisan('weather:get', [
            'zip' => '80110',
        ]);

        $this->assertTrue(true);

    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeatherDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 60);
            $table->string('zip', 5)->index();
            $table->string('conditions', 255)->nullable();
            $table->string('conditions_short', 60)->nullable();
            $table->decimal('pressure', 6,2)->nullable();
            $table->decimal('temperature', 4,1)->nullable();
            $table->decimal('wind_direction', 4,1)->nullable();
            $table->decimal('wind_speed', 4,1)->nullable();
            $table->decimal('humidity', 4,1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_data');
    }
}
